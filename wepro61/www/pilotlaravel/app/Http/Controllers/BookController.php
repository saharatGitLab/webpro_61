<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookController extends Controller
{
    //
//    function store(Request $request){
//        $book = new Books();
//        $book->setAttribute('name',$request->name);
//        $book->setAttribute('category',$request->category);
//        $book->setAttribute('description',$request->description);
//
////        $books->name = $request->name;
////        $books->category = $request->category;
////        $books->description = $request->description;
//
//        if ($book->save()){
//            return true;
//        }
//    }
    function store(Request $request){
        $book = new Books();
        if (Books::Create($request->all())){
            return true;
        }
    }
    function update(Request $request, Books $books){
        if ($books->fill($request->all())->save()){
            return true;
        }
    }
    function index(){
        $books = Books::all();
        return $books;
    }
    function show(Books $books){
        return $books;
    }
    function destroy(Books $books){
        if ($books->delete()) {
            return true;
        }
    }
}

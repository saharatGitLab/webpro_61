<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    //
    protected $table = 'books'; //ORM  Object Reletional model
//    protected $primaryKey = 'isbn_no';
    protected $fillable = ['name', 'category', 'description'];
}
